import Axios from "axios";


export const saveUser = (user) =>{
    Axios.post('http://localhost:3000/api/user', user)
}

export const loadUser = () =>
    Axios.get('http://localhost:3000/api/users')

export const destroyUser = (id) =>
    Axios.delete(`http://localhost:3000/api/users/${id}`)

export const updateUser = (user) =>
    Axios.put(`http://localhost:3000/api/users/${user.id}`, user)