describe("tp vendredi", () => {
  beforeEach(() => {});
  it("tp", () => {
    cy.fixture("users").then((data) => {
      let datas = data.users;
      datas.forEach((userdata) => {
        cy.visit("/");

        cy.get("#firstName")
          .type(userdata.firstName)
          .should("have.value", userdata.firstName);

        cy.get("#lastName")
          .type(userdata.lastName)
          .should("have.value", userdata.lastName);

        if (userdata.gender == "male") {
          cy.get(".form-body > :nth-child(4)")
            .click();
        } else if (userdata.gender == "female") {
          cy.get(".form-body > :nth-child(5)")
            .click();
        } else {
          cy.get(".form-body > :nth-child(6)")
            .click();
        }

        cy.get("#email")
          .type(userdata.email)
          .should("have.value", userdata.email);

        cy.get("#password")
          .type(userdata.password)
          .should("have.value", userdata.password);

        cy.get("#confirmPassword")
          .type(userdata.password, { force: true })
          .should("have.value", userdata.password);

        cy.get(".btn").click();
      });
    });
  });

  it("Save Data", () => {
    cy.server();
    cy.route({
      url: "/api/user",
      method: "POST",
      status: 201,
      response: {},
    }).as("addUser");
    cy.visit("/");

    const data = {
      firstName: "Nicolas",
      lastName: "Gervaux",
      email: "Nicolas.gervaux@gmail.com",
      password: "motdepasse",
      confirmPassword: "motdepasse",
    };

    cy.get("#firstName")
      .type(data.firstName)
      .should("have.value", data.firstName);
    cy.get("#lastName")
      .type(data.lastName)
      .should("have.value", data.lastName);
    cy.get("#email")
      .type(data.email)
      .should("have.value", data.email);
    cy.get("#password")
      .type(data.password)
      .should("have.value", data.password);
    cy.get("#confirmPassword")
      .type(data.confirmPassword)
      .should("have.value", data.confirmPassword);

    cy.get(".btn").click();

    cy.wait("@addUser").then(() => {
      cy.fixture("fakeUser.json")
        .then((userFixture) => {
          userFixture.push(data);
          cy.writeFile(
            "cypress/fixtures/fakeUser.json",
            JSON.stringify(userFixture)
          );
      });
    });
  });
});